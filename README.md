halvtone composer packages
==========================

Version
-------
1.1.0

Introduction
------------
This is a [composer](http://getcomposer.org "composer - package manager") repository.

How to use?
-----------
Simply add the following lines to your project's composer.json.

	"repositories": [{
    	"type": "composer",
    	"url": "https://gitlab.com/halvtone/composer-packages/raw/master"
    }]

Packages
--------
<table>
  <tr>
    <th>Package</th><th>Version</th>
  </tr>
  <tr>
    <td>dojo/toolkit</td><td>1.8.1</td>
  </tr>
  <tr>
    <td>dojo/toolkit</td><td>1.8.3</td>
  </tr>
  <tr>
    <td>dojo/toolkit</td><td>1.9.0</td>
  </tr>
  <tr>
    <td>dojo/toolkit</td><td>1.9.1</td>
  </tr>
  <tr>
    <td>dojo/toolkit</td><td>1.9.2</td>
  </tr>
  <tr>
    <td>dojo/toolkit</td><td>1.10.0</td>
  </tr>
  <tr>
    <td>dojo/toolkit</td><td>1.10.1</td>
  </tr>
  <tr>
    <td>dojo/toolkit</td><td>1.10.3</td>
  </tr>
</table>
